using System;
using System.Collections.Generic;
using TesteViaVarejo;
using Xunit;
using System.Linq;
namespace TesteViaVarejoTesteUnitario
{
    public class Teste1
    {

        [Fact]
        public void ChecarNumeroVisinhosdosFilhos()
        {
            Localizacao a = new Localizacao(3, 3);
            List<Localizacao> lst = new List<Localizacao>();
            lst.Add(new Localizacao(1, 1));
            lst.Add(new Localizacao(2, 2));
            lst.Add(new Localizacao(2, 3));
            lst.Add(new Localizacao(3, 5));
            lst.Add(new Localizacao(4, 4));
            lst.Add(new Localizacao(5, 4));
            lst.Add(new Localizacao(5, 2));
            lst.Add(new Localizacao(1, 6));
            lst.Add(new Localizacao(5, 1));
            IServicoLocalizar servico = new LocalizarPontosService();
            var vizinhos = servico.LocalizarVizinhosProximos(lst, a, new DistanciaPorEuclides());
            var visinhosDosVisinhos = vizinhos.PegarMeusVizinhos();
            int numerosVizinhosEsperados = 3;

            foreach (var i in visinhosDosVisinhos)
            {
                Int32 numeroVizinhos = i.PegarMeusVizinhos().Count;
                Assert.Equal(numeroVizinhos, numerosVizinhosEsperados);
            }
        }



        [Fact]
        public void ChecarVizinhoProximo()
        {
            Localizacao a = new Localizacao(3, 3);
            List<Localizacao> lst = new List<Localizacao>();
            lst.Add(new Localizacao(1, 1));
            lst.Add(new Localizacao(2, 2));
            lst.Add(new Localizacao(2, 3));
            lst.Add(new Localizacao(3, 5));
            lst.Add(new Localizacao(4, 4));
            lst.Add(new Localizacao(5, 4));
            lst.Add(new Localizacao(5, 2));
            lst.Add(new Localizacao(1, 6));
            lst.Add(new Localizacao(5, 1));
            IServicoLocalizar servico = new LocalizarPontosService();
            var vizinhos = servico.LocalizarVizinhosProximos(lst, a, new DistanciaPorEuclides());

            Localizacao[] p = vizinhos.PegarMeusVizinhos().ToArray();

            bool _meuVizinhoEIgual = (p[0] == new Localizacao(2, 3));
            Assert.Equal(_meuVizinhoEIgual, true );

            _meuVizinhoEIgual = (p[1] == new Localizacao(2, 2));
            Assert.Equal(_meuVizinhoEIgual, true);

            _meuVizinhoEIgual = (p[2] == new Localizacao(4, 4));
            Assert.Equal(_meuVizinhoEIgual, true);
        }


        [Fact]
        public void ChecarNumeroVizinhos()
        {
            Localizacao a = new Localizacao(3, 3);
            List<Localizacao> lst = new List<Localizacao>();
            lst.Add(new Localizacao(1, 1));
            lst.Add(new Localizacao(2, 2));
            lst.Add(new Localizacao(2, 3));
            lst.Add(new Localizacao(3, 5));
            lst.Add(new Localizacao(4, 4));
            lst.Add(new Localizacao(5, 4));
            lst.Add(new Localizacao(5, 2));
            lst.Add(new Localizacao(1, 6));
            lst.Add(new Localizacao(5, 1));
            IServicoLocalizar servico = new LocalizarPontosService();
            var vizinhos = servico.LocalizarVizinhosProximos(lst, a, new DistanciaPorEuclides());
            int numeroVizinhos = vizinhos.PegarMeusVizinhos().Count;
            int numerosVizinhosEsperados = 3;
            Assert.Equal(numeroVizinhos, numerosVizinhosEsperados);
        }
    }
}
