﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
namespace TesteViaVarejo
{
    public class DistanciaPorEuclides : IMetodoCalcularDistancia
    {
        public double CalcularDistancia(Localizacao pointA, Localizacao pointB)
        {
            double _X = Math.Pow(pointA.X - pointB.X, 2);
            double _Y = Math.Pow(pointA.Y - pointB.Y, 2);
            return Math.Sqrt(_X + _Y);
        }
    }

    public class LocalizarPontosService : IServicoLocalizar
    {
        public Vizinhos LocalizarVizinhosProximos(List<Localizacao> localizacaoVizinhos, Localizacao no, IMetodoCalcularDistancia metodo , int numerovizinhos = 3 )
        {
            Vizinhos objLocalizacao = new Vizinhos(no);
            var lista = localizacaoVizinhos.ToList();
            Procurar(lista, ref objLocalizacao, metodo, 0, numerovizinhos);

            var vizinhos = objLocalizacao.PegarMeusVizinhos();

            for (int i = 0; i < vizinhos.Count; i++)
            {
                lista = localizacaoVizinhos.ToList();
                var item = vizinhos[i];
                lista.Remove(item);
                Procurar(lista, ref item, metodo, 0, numerovizinhos);
            }

            return objLocalizacao;
        }

        private void Procurar(List<Localizacao> localizacaoVizinhos, ref Vizinhos no, IMetodoCalcularDistancia metodo , int currentIndex, int numeroMaximoVizinhos)
        {
            if (currentIndex == numeroMaximoVizinhos)
                return;

            double distance = double.MaxValue;
            int indice = -1;
            for (int i = 0; i < localizacaoVizinhos.Count; i++)
            {
                var currentValue = metodo.CalcularDistancia(no, localizacaoVizinhos[i]);
                if (currentValue < distance)
                {
                    distance = currentValue;
                    indice = i;
                }
            }

            no.AdicionarPontoProximo(new Vizinhos(localizacaoVizinhos[indice]));
            localizacaoVizinhos.RemoveAt(indice);
            currentIndex += 1;
            Procurar(localizacaoVizinhos, ref no, metodo, currentIndex, numeroMaximoVizinhos);
        }
    }

    public class Program
    {
        static void Main(string[] args)
        {
            Localizacao a = new Localizacao(3,3);
            Localizacao a2 = new Localizacao(3, 3);
            List<Localizacao> lst = new List<Localizacao>();
            lst.Add(new Localizacao(1, 1));
            lst.Add(new Localizacao(2, 2));
            lst.Add(new Localizacao(2, 3));
            lst.Add(new Localizacao(3, 5));
            lst.Add(new Localizacao(4, 4));
            lst.Add(new Localizacao(5, 4));
            lst.Add(new Localizacao(5, 2));
            lst.Add(new Localizacao(1, 6));
            lst.Add(new Localizacao(5, 1));
            IServicoLocalizar servico = new LocalizarPontosService();
            var vizinhos = servico.LocalizarVizinhosProximos(lst, a, new DistanciaPorEuclides());
        }
    }
}
