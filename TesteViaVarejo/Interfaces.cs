﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TesteViaVarejo
{
    public interface IMetodoCalcularDistancia
    {
        double CalcularDistancia(Localizacao pointA, Localizacao pointB);
    }

    public interface IServicoLocalizar
    {
        Vizinhos LocalizarVizinhosProximos(List<Localizacao> lst, Localizacao no, IMetodoCalcularDistancia metodo, int numerovizinhos=3);
    }
}
