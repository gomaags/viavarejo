﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TesteViaVarejo
{
    public class Localizacao
    {
        public int X { get; set; }
        public int Y { get; set; }
        public Localizacao(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public static bool operator !=(Localizacao p1, Localizacao p2)
        {
            return !((p1.X == p2.X) && (p1.Y == p2.Y));
        }
        public static bool operator ==(Localizacao p1, Localizacao p2)
        {
            return ((p1.X == p2.X) && (p1.Y == p2.Y));
        }
    }
    


    public class Vizinhos : Localizacao
    {
        List<Vizinhos> pontosProximos = new List<Vizinhos>();
        public void AdicionarPontoProximo(Vizinhos ponto)
        {
            pontosProximos.Add(ponto);
        }
        public List<Vizinhos> PegarMeusVizinhos()
        {
            return pontosProximos;
        }

        public Vizinhos(Localizacao localizacao) : base(localizacao.X, localizacao.Y)
        {

        }
    }
}
